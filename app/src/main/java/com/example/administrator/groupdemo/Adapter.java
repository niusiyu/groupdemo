package com.example.administrator.groupdemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;


/**
 * Created by zhangxutong .
 * Date: 16/08/28
 */

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private Context mContext;
    private List<Bean> mDatas;
    private LayoutInflater mInflater;


    public Adapter(Context mContext, List<Bean> mDatas) {
        this.mContext = mContext;
        this.mDatas = mDatas;
        mInflater = LayoutInflater.from(mContext);
    }

    public void setData(List<Bean> mDatas) {
        this.mDatas = mDatas;
        notifyDataSetChanged();
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(final Adapter.ViewHolder holder, final int position) {
        final Bean item = mDatas.get(position);
        Glide.with(mContext).load(item.getUrl()).into(holder.item_iv);
    }

    @Override
    public int getItemCount() {
        return mDatas != null ? mDatas.size() : 0;
    }

    public Bean getItem(int position) {
        return mDatas.get(position);
    }

    public List<Bean> getDatas() {
        return mDatas;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView item_iv;

        public ViewHolder(View itemView) {
            super(itemView);
            item_iv = itemView.findViewById(R.id.item_iv);
        }
    }

}
